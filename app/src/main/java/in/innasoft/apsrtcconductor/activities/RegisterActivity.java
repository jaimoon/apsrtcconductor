package in.innasoft.apsrtcconductor.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import in.innasoft.apsrtcconductor.R;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    TextView reg_tittle,reg_haveaccount,reg_loginhere;
    TextInputLayout reg_name_til,reg_email_til,reg_phone_til,reg_password_til,reg_cnfpassword_til;
    EditText reg_name_edt,reg_email_edt,reg_phone_edt,reg_password_edt,reg_cnfpassword_edt;
    Button reg_cancel,reg_register;
    Typeface typeface,typeface2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        typeface = Typeface.createFromAsset(getAssets(), "aquawaxlight.ttf");
        typeface2 = Typeface.createFromAsset(getAssets(), "opensansregular.ttf");

        reg_name_til = (TextInputLayout) findViewById(R.id.reg_name_til);
        reg_name_til.setTypeface(typeface2);

        reg_email_til = (TextInputLayout) findViewById(R.id.reg_email_til);
        reg_email_til.setTypeface(typeface2);

        reg_phone_til = (TextInputLayout) findViewById(R.id.reg_phone_til);
        reg_phone_til.setTypeface(typeface2);

        reg_password_til = (TextInputLayout) findViewById(R.id.reg_password_til);
        reg_password_til.setTypeface(typeface2);

        /*reg_cnfpassword_til = (TextInputLayout) findViewById(R.id.reg_cnfpassword_til);
        reg_cnfpassword_til.setTypeface(typeface2);*/

        reg_tittle = (TextView) findViewById(R.id.reg_tittle);
        reg_tittle.setTypeface(typeface2);

        reg_haveaccount = (TextView) findViewById(R.id.reg_haveaccount);
        reg_haveaccount.setTypeface(typeface2);

        reg_loginhere = (TextView) findViewById(R.id.reg_loginhere);
        reg_loginhere.setTypeface(typeface2);
        reg_loginhere.setOnClickListener(this);

        reg_name_edt = (EditText) findViewById(R.id.reg_name_edt);
        reg_name_edt.setTypeface(typeface2);

        reg_email_edt = (EditText) findViewById(R.id.reg_email_edt);
        reg_email_edt.setTypeface(typeface2);

        reg_phone_edt = (EditText) findViewById(R.id.reg_phone_edt);
        reg_phone_edt.setTypeface(typeface2);

        reg_password_edt = (EditText) findViewById(R.id.reg_password_edt);
        reg_password_edt.setTypeface(typeface2);

        /*reg_cnfpassword_edt = (EditText) findViewById(R.id.reg_cnfpassword_edt);
        reg_cnfpassword_edt.setTypeface(typeface2);*/

        reg_cancel = (Button) findViewById(R.id.reg_cancel);
        reg_cancel.setTypeface(typeface2);
        reg_cancel.setOnClickListener(this);

        reg_register = (Button) findViewById(R.id.reg_register);
        reg_register.setTypeface(typeface2);
        reg_register.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (v == reg_cancel){
            finish();
        }

        if (v == reg_register){

            //validate();

            Intent intent = new Intent(this,LoginActivity.class);
            startActivity(intent);
        }

        if (v == reg_loginhere){

            Intent intent = new Intent(this,LoginActivity.class);
            startActivity(intent);
        }

    }
}
