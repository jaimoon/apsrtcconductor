package in.innasoft.apsrtcconductor.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import in.innasoft.apsrtcconductor.R;
import in.innasoft.apsrtcconductor.utilities.UserSessionManager;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    TextView login_tittle,login_donthaveaccount,login_registerhere;
    TextInputLayout login_name_til,login_password_til;
    EditText login_name_edt,login_password_edt;
    Button login_cancel,login_login;
    Typeface typeface,typeface2;
    UserSessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        session = new UserSessionManager(getApplicationContext());

        typeface = Typeface.createFromAsset(getAssets(), "aquawaxlight.ttf");
        typeface2 = Typeface.createFromAsset(getAssets(), "opensansregular.ttf");

        login_name_til = (TextInputLayout) findViewById(R.id.login_name_til);
        login_name_til.setTypeface(typeface2);

        login_password_til = (TextInputLayout) findViewById(R.id.login_password_til);
        login_password_til.setTypeface(typeface2);

        login_tittle = (TextView) findViewById(R.id.login_tittle);
        login_tittle.setTypeface(typeface2);

        login_donthaveaccount = (TextView) findViewById(R.id.login_donthaveaccount);
        login_donthaveaccount.setTypeface(typeface2);

        login_registerhere = (TextView) findViewById(R.id.login_registerhere);
        login_registerhere.setTypeface(typeface2);
        login_registerhere.setOnClickListener(this);

        login_name_edt = (EditText) findViewById(R.id.login_name_edt);
        login_name_edt.setTypeface(typeface2);

        login_password_edt = (EditText) findViewById(R.id.login_password_edt);
        login_password_edt.setTypeface(typeface2);

        login_cancel = (Button) findViewById(R.id.login_cancel);
        login_cancel.setTypeface(typeface2);
        login_cancel.setOnClickListener(this);

        login_login = (Button) findViewById(R.id.login_login);
        login_login.setTypeface(typeface2);
        login_login.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        if (v == login_login){
            if(login_name_edt.getText().toString().trim().length() > 0 || login_password_edt.getText().toString().trim().length() > 0) {
                //validate();
                String username = login_name_edt.getText().toString();
                String password = login_password_edt.getText().toString();
                if(username.equals("admin") && password.equals("admin")) {
                    Intent intent = new Intent(this, ChooseCategoryActivity.class);
                    startActivity(intent);
                    session.createUserLoginSession();
                    finish();
                }else {
                    Toast.makeText(getApplicationContext(), "Invalid Username/Password.", Toast.LENGTH_SHORT).show();
                }
            }else {
                Toast.makeText(getApplicationContext(), "Please Enter Username/Password.", Toast.LENGTH_SHORT).show();
            }
        }

        if (v == login_cancel){

           finish();
        }

        if (v == login_registerhere){

            Intent intent = new Intent(this,RegisterActivity.class);
            startActivity(intent);
        }

    }
}
