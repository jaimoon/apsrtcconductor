package in.innasoft.apsrtcconductor.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.innasoft.apsrtcconductor.R;
import in.innasoft.apsrtcconductor.utilities.UserSessionManager;

public class ChooseCategoryActivity extends AppCompatActivity implements View.OnClickListener {

    TextView cc_tittle,logout_tv;
    Button cc_checking,cc_history;

    Typeface typeface,typeface2;
    UserSessionManager userSessionManager;

    private static final int REQ_SCAN = 0;
    TextView display_student_informations;
    String GETTING_STUDENT_DETAILS = "http://learningslot.in/apsrtc/qrreader/retrieve.php";
    ImageView student_image_disp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_category);

        userSessionManager = new UserSessionManager(getApplicationContext());

        typeface = Typeface.createFromAsset(getAssets(), "aquawaxlight.ttf");
        typeface2 = Typeface.createFromAsset(getAssets(), "opensansregular.ttf");

        cc_tittle = (TextView) findViewById(R.id.cc_tittle);
        cc_tittle.setTypeface(typeface2);

        student_image_disp = (ImageView) findViewById(R.id.student_image_disp);

        logout_tv = (TextView) findViewById(R.id.logout_tv);
        logout_tv.setTypeface(typeface2);


        display_student_informations = (TextView) findViewById(R.id.display_student_informations);
        display_student_informations.setTypeface(typeface2);

        cc_checking = (Button) findViewById(R.id.cc_checking);
        cc_checking.setTypeface(typeface2);
        cc_checking.setOnClickListener(this);

        cc_history = (Button) findViewById(R.id.cc_history);
        cc_history.setTypeface(typeface2);
        cc_history.setOnClickListener(this);

        logout_tv.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        if (v == cc_checking){
            Intent i = new Intent(ChooseCategoryActivity.this, ScannerActivity.class);
            startActivityForResult(i,REQ_SCAN);
        }

        if (v == cc_history){

        }

        if (v == logout_tv){
            userSessionManager.logoutUser();
            Toast.makeText(getApplicationContext(), "Logout Successfully....!", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Bundle result = data.getExtras();
            final String s = result.getString("result");

            final StringRequest stringRequest = new StringRequest(Request.Method.POST, GETTING_STUDENT_DETAILS,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                Log.d("RESPOJNCEVALUE" ,response);
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                String name = jsonObject1.getString("name");
                                String phone = jsonObject1.getString("phone");
                                String email = jsonObject1.getString("email");
                                String uniqueId = jsonObject1.getString("uniqueid");
                                String imageUrl = "http://learningslot.in/apsrtc/qrreader/uploads/"+jsonObject1.getString("image");
                               Log.d("IMAGEURL", imageUrl);
                                String validity_status = jsonObject1.getString("validity_status");
                                String from_date = jsonObject1.getString("from_date");
                                String to_date = jsonObject1.getString("to_date");

                                final Dialog dialog = new Dialog(ChooseCategoryActivity.this);
                                dialog.setContentView(R.layout.student_details_dialog);
                                dialog.setTitle("Pass Information");

                                Button dialogButtonOk = (Button) dialog.findViewById(R.id.customDialogOk);
                                TextView studentDetailesTV = (TextView) dialog.findViewById(R.id.student_details);
                                studentDetailesTV.setTypeface(typeface2);
                                studentDetailesTV.setText("\tName : "+name+"\n\tPhone : "+phone+"\n\tEmail : "+email+"\n\tStudent ID :"+uniqueId+"\n\tValidity :"+from_date+" To "+to_date);
                                ImageView passStatusIV = (ImageView) dialog.findViewById(R.id.status_of_pass_iv);
                                if(validity_status.equals("0"))
                                {
                                    passStatusIV.setImageResource(R.drawable.cross);
                                }
                                if(validity_status.equals("1")) {
                                    passStatusIV.setImageResource(R.drawable.richt);
                                }
                                ImageView studentPhoto = (ImageView) dialog.findViewById(R.id.student_image);
                                Picasso.with(getApplicationContext()).load(imageUrl)
                                        .fit()
                                        .placeholder(R.drawable.profile)
                                        .into(studentPhoto);

                                dialogButtonOk.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.dismiss();
                                    }
                                });

                                dialog.show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }

            ){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("uniqueid", s);
                    return params;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);

        }
    }
}
