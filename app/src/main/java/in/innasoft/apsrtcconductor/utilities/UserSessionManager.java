package in.innasoft.apsrtcconductor.utilities;

/**
 * Created by User on 7/29/2016.
 */

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.util.HashMap;

import in.innasoft.apsrtcconductor.activities.LoginActivity;


public class UserSessionManager {

    // Shared Preferences reference
    SharedPreferences pref;

    // Editor reference for Shared preferences
    Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREFER_NAME = "apsrtc_conductor";


    // All Shared Preferences Keys
    private static final String IS_USER_LOGIN = "IsUserLoggedIn";

    // User name (make variable public to access from outside)
   // public static final String KEY_ACCSES = "access_key";

   // public static final String USER_ID = "user_id";
    //public static final String USER_NAME = "user_name";
   // public static final String USER_MOBILE = "user_mobile";
   // public static final String USER_EMAIL = "user_email";
   // public static final String USER_AGE = "age";

    // Email address (make variable public to access from outside)
    //public static final String KEY_EMAIL = "email";

    // Constructor
    public UserSessionManager(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    //Create login session
    //public void createUserLoginSession(String key, String userId, String userName, String mobile, String age, String email){
    public void createUserLoginSession(){
        // Storing login value as TRUE
        editor.putBoolean(IS_USER_LOGIN, true);

        // Storing name in pref
        //editor.putString(KEY_ACCSES, key);
       // editor.putString(USER_ID,userId);
        //editor.putString(USER_NAME, userName);
        //editor.putString(USER_MOBILE, mobile);
       // editor.putString(USER_EMAIL, email);
        //editor.putString(USER_AGE, age);



        // Storing email in pref
        // editor.putString(KEY_EMAIL, email);

        // commit changes
        editor.commit();
    }


    public boolean checkLogin(){
        // Check login status
        if(!this.isUserLoggedIn()){

            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, LoginActivity.class);

            // Closing all the Activities from stack
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(i);

            return true;
        }
        return false;
    }



    /**
     * Get stored session data
     * */
    public HashMap<String, String> getUserDetails(){

        //Use hashmap to store user credentials
        HashMap<String, String> user = new HashMap<String, String>();

        // user name
       // user.put(KEY_ACCSES, pref.getString(KEY_ACCSES, null));
        //user.put(USER_ID, pref.getString(USER_ID, null));
        //user.put(USER_NAME, pref.getString(USER_NAME, null));
        //user.put(USER_MOBILE, pref.getString(USER_MOBILE, null));
       // user.put(USER_AGE, pref.getString(USER_AGE, null));
       // user.put(USER_EMAIL, pref.getString(USER_EMAIL, null));



        // user email id
        // user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));

        // return user
        return user;
    }


    public void logoutUser(){

        editor.clear();
        editor.commit();

        // After logout redirect user to Login Activity
        Intent i = new Intent(_context, LoginActivity.class);

        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        _context.startActivity(i);
    }


    // Check for login
    public boolean isUserLoggedIn(){
        return pref.getBoolean(IS_USER_LOGIN, false);
    }
}